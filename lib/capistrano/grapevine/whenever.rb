require 'capistrano/grapevine/common'

configuration = Capistrano::Configuration.respond_to?(:instance) ?
  Capistrano::Configuration.instance(:must_exist) :
  Capistrano.configuration(:must_exist)

configuration.load do
    require "whenever/capistrano"
    set :whenever_roles, :cron

    if binstubs
        set :whenever_command, "bin/whenever"
    else
        set :whenever_command, "bundle exec whenever"
    end

end
