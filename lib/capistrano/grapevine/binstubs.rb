require 'capistrano/grapevine/common'

configuration = Capistrano::Configuration.respond_to?(:instance) ?
  Capistrano::Configuration.instance(:must_exist) :
  Capistrano.configuration(:must_exist)

# shebang requires bundler >= 1.1
configuration.load do
    set :binstubs, true
    _cset :bundle_flags, "--deployment --quiet --binstubs"
    set :rake, "bin/rake"
end
