require 'capistrano/grapevine/common'

configuration = Capistrano::Configuration.respond_to?(:instance) ?
  Capistrano::Configuration.instance(:must_exist) :
  Capistrano.configuration(:must_exist)

configuration.load do

  after "assets:precompile", "resque:copy_assets"
  after "deploy:restart", "resque:restart"

  namespace :resque do

    task :restart, :roles => :app do
      run "sudo /etc/init.d/resque_#{application} restart"
    end

    task :copy_assets, :roles => :app do
      target = File.join(release_path, 'public', 'hq', 'resque')
      run "mkdir #{ release_path }/public/hq && cp -r `cd #{release_path} && bundle show resque`/lib/resque/server/public #{target}"
    end

  end

end
