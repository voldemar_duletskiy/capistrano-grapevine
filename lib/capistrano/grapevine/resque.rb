require 'capistrano/grapevine/common'

configuration = Capistrano::Configuration.respond_to?(:instance) ?
  Capistrano::Configuration.instance(:must_exist) :
  Capistrano.configuration(:must_exist)

configuration.load do

  before "deploy:symlink", "resque:copy_assets"
  after "deploy:restart", "resque:stop"

  namespace :resque do

    task :stop, :roles => :app do
      run "cd #{release_path}; RAILS_ENV=#{ rails_env } #{rake} resque:stop"
      # will be started by monit
    end

    task :copy_assets, :roles => :app do
      target = File.join(release_path, 'public', 'hq', 'resque')
      run "mkdir #{ release_path }/public/hq && cp -r `cd #{release_path} && bundle show resque`/lib/resque/server/public #{target}"
    end

  end

end
