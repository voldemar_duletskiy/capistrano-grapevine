require 'capistrano/grapevine/common'

configuration = Capistrano::Configuration.respond_to?(:instance) ?
  Capistrano::Configuration.instance(:must_exist) :
  Capistrano.configuration(:must_exist)

configuration.load do

    _cset :user, "sites"
    _cset(:group) { user }

    set :scm, :git
    set(:repository) { "git@github.com:greengrape/#{application}.git" }
    _cset :scm_verbose, true
    set :deploy_via, :remote_cache
    set(:deploy_to) { "/home/#{user}/#{application}" }
    _cset :symlink_dirs, []

    _cset :use_sudo, false
    default_run_options[:pty] = true
    ssh_options[:forward_agent] = true

    set :default_environment, {
        "PATH" => "/home/#{user}/.rbenv/shims:/home/#{user}/.rbenv/bin:$PATH"
    }

    set :binstubs, false
    set :rake, "bundle exec rake"


    after "deploy:symlink", "deploy:misc:symlink"

    namespace :deploy do
        task :start do
            run "sudo /etc/init.d/unicorn_#{ application } start"
        end

        task :stop do
            run "/etc/init.d/unicorn_#{ application } stop"
        end

        task :restart do
            run "sudo /etc/init.d/unicorn_#{ application} reload"
        end

        namespace :web do

            task :disable, :roles => :app do
                on_rollback { run "rm #{current_path}/public/maintenance.html; exit 0" }
                run "ln -s #{current_path}/public/_maintenance.html #{current_path}/public/maintenance.html; true"
            end

            task :enable, :roles => :app do
                run "rm #{current_path}/public/maintenance.html; true"
            end

        end

        namespace :misc do
            task :symlink, :roles => :app do
                (symlink_dirs || []).each do |linked_dir|
                run "rm -rf #{release_path}/#{ linked_dir }"
                run "ln -nfs #{shared_path}/#{ linked_dir } #{release_path}/#{ linked_dir }"
                end
            end
        end

    end

    namespace :remote do
        task :rake, :roles => :app do
            run "cd #{deploy_to}/current; bin/rake #{ ENV['task'] } RAILS_ENV=#{ rails_env }"
        end
    end



end
