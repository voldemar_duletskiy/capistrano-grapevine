require 'capistrano/grapevine/common'

configuration = Capistrano::Configuration.respond_to?(:instance) ?
  Capistrano::Configuration.instance(:must_exist) :
  Capistrano.configuration(:must_exist)

configuration.load do

  set :symlink_dirs, (symlink_dirs + ['db/sphinx'])

  after "deploy:restart", "sphinx:configure"
  after "deploy:restart", "sphinx:rebuild"

  before "sphinx:rebuild", "deploy:web:disable"
  after "sphinx:rebuild", "deploy:web:enable"

  namespace :sphinx do

    task :stop do
      run "cd #{current_path}; RAILS_ENV=#{ rails_env } #{rake} ts:stop"
    end

    task :configure do
      run "cd #{current_path} && RAILS_ENV=#{ rails_env } #{rake} ts:configure"
    end

    task :rebuild do
      run "cd #{current_path} && RAILS_ENV=#{ rails_env } #{rake} ts:rebuild"
    end

  end

end
