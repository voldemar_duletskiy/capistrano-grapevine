require 'capistrano/grapevine/common'

configuration = Capistrano::Configuration.respond_to?(:instance) ?
  Capistrano::Configuration.instance(:must_exist) :
  Capistrano.configuration(:must_exist)

configuration.load do

  after "deploy:update_code", "assets:precompile"

  namespace :assets do

    task :precompile, :roles => :app do
      from = previous_revision

      force = ENV['FORCE_ASSETS']

      assets_changed = capture("cd #{release_path} && #{source.local.log(from)} app/assets/ | wc -l").to_i > 0
      previous_packed = capture("ls -1 #{previous_release}/public/assets | wc -l").to_i > 0

      if force || assets_changed || !previous_packed
        run %Q{cd #{release_path} && RAILS_ENV=#{rails_env} #{rake} assets:precompile}
      else
        run %Q(cp -R #{previous_release}/public/assets #{release_path}/public/assets)
        logger.info "Skipping asset pre-compilation because there were no asset changes"
      end
    end

  end


end
